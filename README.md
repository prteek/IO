# IO
#### Random projects

[1. Dash coin mining contract analysis](https://nbviewer.jupyter.org/github/prteek/IO/blob/master/genesis_metrics.ipynb)  
[2. Personal Polynomial](https://nbviewer.jupyter.org/github/prteek/IO/blob/master/PersonalPolynomial.ipynb)  
[3. Twitter Data Analysis](https://nbviewer.jupyter.org/github/prteek/IO/blob/master/twitterDataAnalysis.ipynb)  
[4. World Bank Data Analysis](https://nbviewer.jupyter.org/github/prteek/IO/blob/master/WorldBankDataPlots.ipynb)  
[5. Document Scanner](https://nbviewer.jupyter.org/github/prteek/IO/blob/master/scanner.ipynb)  
