# IO
#### Random projects

[1. Dash coin mining contract analysis](https://nbviewer.jupyter.org/urls/gitlab.com/prteek/IO/-/raw/master/genesis_metrics.ipynb)  
[2. Personal Polynomial](https://nbviewer.jupyter.org/urls/gitlab.com/prteek/IO/-/raw/master/PersonalPolynomial.ipynb)  
[3. Twitter Data Analysis](https://nbviewer.jupyter.org/urls/gitlab.com/prteek/IO/-/raw/master/twitterDataAnalysis.ipynb)  
[4. World Bank Data Analysis](https://nbviewer.jupyter.org/urls/gitlab.com/prteek/IO/-/raw/master/WorldBankDataPlots.ipynb)  
[5. Document Scanner](https://nbviewer.jupyter.org/urls/gitlab.com/prteek/IO/-/raw/master/scanner.ipynb)  
